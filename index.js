
      function splitArray(pArray)
      {
        if (!(this instanceof splitArray)){
            return new splitArray(pArray);
       }

       function calculateNumbers(a, b)
       {
         var multi = a*b;
         var add = a+b;

           if(multi > add)
           {
             return true; //If true then create pair
           }
           else if(add > multi)
           {
             return false;//If false then separate
           }
           else if(add === multi)
           {
             return true;
           }
           else
           {
             throw new Error('Error');
           }
       }

       function compareNumbers(a, b)
       {
         return b - a;
       }

       function compareNegativeNumbers(a,b)
       {
         return a - b;
       }

       function checkForNegativeNumbers(array)
       {
         var result = false;
         var arr = array;
         arr.sort(compareNumbers);

         arr.forEach(function(item)
        {
          if(item < 0)
          {
            result = true;
          }
          else
          {
            result = false;
          }
        });

          return result;
       }


      if(checkForNegativeNumbers(pArray))
      {
        this.array = pArray.sort(compareNegativeNumbers);
      }
      else
      {
        this.array = pArray.sort(compareNumbers);
      }



        //TODO: Separate intergers into pairs
        this.splitPairs = function() {
          var pairs = [];
          var calculatedNumbers;

          for (var i=0 ; i <this.array.length ; i+=2) {
              if (this.array[i+1] !== undefined)
              {
                if(calculateNumbers(this.array[i],this.array[i+1]))
                {
                  pairs.push([this.array[i], this.array[i+1]]);
                }
                else
                {
                  pairs.push([this.array[i]]);
                  pairs.push ([this.array[i+1]]);
                }
              }
              else
              {
                  pairs.push ([this.array[i]]);
              }
          }
          return pairs;
      };


        //TODO: Multiple pairs
       this.multiplePairs = function()
       {
         var array = this.splitPairs();
         var multipleArray = [];

         array.forEach(function(item)
          {
            if(item.length === 2)
            {
            multipleArray.push(item[0]*item[1]);
            }
            else
            {
            multipleArray.push(item[0]);
            }
          });
         return multipleArray;
       };


        //TODO: Add Pairs Together
        this.addMultipliedNumbers = function()
        {
          var array = this.multiplePairs();
          var i = 0, result = 0;

          while(i < array.length)
          {
            result += array[i];
            i++;
          }

          return result;
        };

      }

module.exports = splitArray;
