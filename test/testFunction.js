var chai = require('chai');
var splitArray = require('../index.js');
var expect = chai.expect;

describe('Intergers', function()
{
  it('should be an array', function()
  {
    var testObject = splitArray([1,2,3,4]);
    expect(testObject.array).to.eql([4,3,2,1]);
  });

  it('should split array into pairs', function()
  {
    var array = splitArray([0,1,2,3,4,5]);
    expect(array.splitPairs()).length.to.be(4);
  });

  it('should multiple pairs', function()
  {
    var array = splitArray([1,2,3,4]);
    var testValue = array.multiplePairs();
    expect(testValue[0]).to.equal(12);
  });

  it('should add multipled pairs', function()
  {
    var array = splitArray([0,1,2,3,4,5]);
    var testValue = array.addMultipliedNumbers();
    expect(testValue).to.equal(27);
  });

  it('should return correct amount', function()
  {
    var array = splitArray([1,1]);
    var testValue = array.addMultipliedNumbers();
    expect(testValue).to.equal(2);
  });

  it('should return correct amount (with negative numbers)', function()
  {
    var array = splitArray([-1,0,1]);
    var testValue = array.addMultipliedNumbers();
    expect(testValue).to.equal(1);
  });

  it('should return correct amount when more than 1 interger equal to 1', function()
  {
    var array = splitArray([1,1,1,1]);
    var testValue = array.addMultipliedNumbers();
    expect(testValue).to.equal(4);
  });
});
